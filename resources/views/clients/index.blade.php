@extends('layout')
@section('content')

<h1>Clients</h1>

<ul>
  @foreach($clients as $client)
   <li> {{ $client->name }} <em class="text-primary">{{ $client->email }} </em> </li>
  @endforeach
</ul>

<hr>

<form action="/clients" method="post">
@csrf
<div class="form-group">
  <label for="exampleInputName">Name </label>
  <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter name">
   @error('name')
     <div class="invalid-feedback">
      {{ $errors->first('name') }}
      
     </div>
   @enderror
</div>
<div class="form-group">
  <label for="exampleInputEmail1">Email </label>
  <input type="email" class="form-control @error('email') is-invalid @enderror"  id="exampleInputEmail1" name="email" aria-describedby="emailHelp" name="email" placeholder="Enter email">
  @error('email')
     <div class="invalid-feedback">
      {{ $errors->first('email') }}
     </div>
   @enderror
</div>
<div class="form-group">
  <select class="custom-select @error('status') is-invalid @enderror" id="inputGroupSelect01" name="status">
    <option selected>Choose...</option>
    <option value="1">Actif</option>
    <option value="0">Inactif</option>
  </select>
  @error('status')
     <div class="invalid-feedback">
      {{ $errors->first('status') }}
     </div>
   @enderror
</div>
<button type="submit" class="btn btn-primary">Ajouter client</button>
</form>
@endsection