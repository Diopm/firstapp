<?php

namespace App\Http\Controllers;
use App\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    public function list(){
       $clients = Client::status();
        return view('clients.index',[
            'clients'=>$clients
        ]); 

    }
    public function store(){
       $data = request()->validate([
            'name' => 'required',
            'email' => 'email|required',
            'status' => 'required|integer'
        ]);
       /*  $name=request('name');
        $email=request('email');
        $status=request('status');

        $client = new client();
        
        $client->name = $name;
        $client->email = $email;
        $client->status = $status;

        $client->save(); */
         Client::create($data);
        return back();
    }
}
